# syntax=docker/dockerfile:1-experimental

FROM alpine as download
ARG VERSION=3.22.0-02
# https://sonatype-download.global.ssl.fastly.net/nexus/3/latest-unix.tar.gz
ARG URL="http://download.sonatype.com/nexus/3/nexus-${VERSION}-unix.tar.gz"
RUN apk add curl && curl "$URL" -Lo /tmp/nexus.tgz
RUN --mount=target=/context \
    mkdir -p /opt/nexus && tar xzf /tmp/nexus.tgz -C /opt/nexus/ --strip-components=1 &&\
    sed -i 's/^application-port.*/application-port=8080/' /opt/nexus/etc/nexus-default.properties &&\
    cp /context/nexus.rc /opt/nexus/bin/nexus.rc

FROM alpine as base
RUN apk --update --no-cache add openjdk8-jre-base nss
ARG USER=nexus
ARG DATA=/var/lib/nexus
RUN addgroup -g 500 ${USER} &&\
    adduser -SD -h ${DATA} -s /bin/sh -G ${USER} -u 500 ${USER} &&\
    mkdir -p ${DATA} && chown ${USER} ${DATA} &&\
    mkdir -p /opt && ln -s ${DATA} /opt/sonatype-work
VOLUME ${DATA}
EXPOSE 8080
ENTRYPOINT ["/opt/nexus/bin/nexus", "run"]


FROM base as main
ARG OSUPDATE
LABEL OSUPDATE=${OSUPDATE}
RUN apk --update --no-cache upgrade
RUN --mount=from=download,source=/opt/nexus,target=/mnt/opt/nexus \
    apk --update --no-cache upgrade &&\
    tar c -C /mnt opt/nexus/ | tar x -C /

